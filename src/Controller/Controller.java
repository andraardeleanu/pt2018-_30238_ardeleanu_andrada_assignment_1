package Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import Model.Monom;
import Model.Operations;
import Model.Polinom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Float.parseFloat;


public class Controller {
    

    @FXML
    private Button derivatePolynomials;

    @FXML
    private Button addPolynomials;

    @FXML
    private Button substractPolynomials;

    @FXML
    private Button integratePolynomials;

    @FXML
    private TextField result;

    @FXML
    private Button multiplyPolynomials;

    @FXML
    private TitledPane title;

    @FXML
    private TextField polynomial1;

    @FXML
    private TextField polynomial2;

    public Controller() {
    }

    @FXML
    private void initialize() {
    }


    @FXML
    void add(ActionEvent event) {


        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();

        Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");

        Matcher m1 = p.matcher(polynomial1.getText().toString());
        while (m1.find()) {
            // System.out.println(m1.group(1) + " " + m1.group(2));
            p1.addTerm(new Monom(parseFloat(m1.group(1)), Integer.parseInt(m1.group(2))));
        }
        Matcher m2 = p.matcher(polynomial2.getText().toString());
        while (m2.find()) {
            //System.out.println(m2.group(2) + " " + m2.group(2));
            p2.addTerm(new Monom(parseFloat(m2.group(1)), Integer.parseInt(m2.group(2))));
        }

        Polinom res = Operations.addPolynomials(p1, p2);
        result.setText(res.monomSort().toString());

    }

    @FXML
    void substract(ActionEvent event) {

        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();

        Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Matcher m1 = p.matcher(polynomial1.getText().toString());
        while (m1.find()) {
            //System.out.println(m1.group(1) + " " + m1.group(2));
            p1.addTerm(new Monom(parseFloat(m1.group(1)), Integer.parseInt(m1.group(2))));
        }
        Matcher m2 = p.matcher(polynomial2.getText().toString());
        while (m2.find()) {
            //System.out.println(m2.group(2) + " " + m2.group(2));
            p2.addTerm(new Monom(parseFloat(m2.group(1)), Integer.parseInt(m2.group(2))));
        }



        Polinom res = Operations.substractPolynomials(p1, p2);
        result.setText(res.monomSort().toString());
    }

    @FXML
    void multiply(ActionEvent event) {
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();

        Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Matcher m1 = p.matcher(polynomial1.getText().toString());
        while (m1.find()) {

            //System.out.println(m1.group(1) + " " + m1.group(2));
            p1.addTerm(new Monom(parseFloat(m1.group(1)), Integer.parseInt(m1.group(2))));
        }
        Matcher m2 = p.matcher(polynomial2.getText().toString());
        while (m2.find()) {
            //System.out.println(m2.group(2) + " " + m2.group(2));
            p2.addTerm(new Monom(parseFloat(m2.group(1)), Integer.parseInt(m2.group(2))));
        }



        Polinom res = Operations.multiplyPolynomials(p1,p2);
        result.setText(res.monomSort().toString());
    }


    @FXML
    void derivate(ActionEvent event) {
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Matcher m1 = p.matcher(polynomial1.getText().toString());
        while (m1.find()) {
            p1.addTerm(new Monom(parseFloat(m1.group(1)), Integer.parseInt(m1.group(2))));
        }
    /*
        while (m2.find()) {
            p2.addTerm(new Monom(Float.parseFloat(m2.group(1)), Integer.parseInt(m2.group(2))));
        }
     */


        Polinom res = Operations.derivePolynomial(p1);
        result.setText(res.monomSort().toString());
    }





    @FXML
    void integrate(ActionEvent event) {
        Polinom p1 = new Polinom();//Polinom.parse(polinom1.getText().toString());
        Polinom p2 = new Polinom();//Polinom.parse(polinom2.getText().toString());

        Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Matcher m1 = p.matcher(polynomial1.getText().toString());
        while (m1.find()) {
            p1.addTerm(new Monom(parseFloat(m1.group(1)), Integer.parseInt(m1.group(2))));
        }
        Matcher m2 = p.matcher(polynomial2.getText().toString());
        /*
        while (m2.find()) {
            System.out.println(m2.group(2) + " " + m2.group(2));
            p2.addTerm(new Monom(Float.parseFloat(m2.group(1)), Integer.parseInt(m2.group(2))));
        }*/
        Polinom res = Operations.integratePolynomials(p1);
        result.setText(res.monomSort().toString());

    }

}


