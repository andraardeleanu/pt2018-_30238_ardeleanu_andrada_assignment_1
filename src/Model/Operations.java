package Model;
import java.util.ArrayList;
import java.util.List;

public class Operations {


    public static Polinom addPolynomials(Polinom p1, Polinom p2) {

        List<Monom> toBeRemoved1 = new ArrayList<>();
        List<Monom> toBeRemoved2 = new ArrayList<>();

        Polinom result = new Polinom();

        for (int i = 0; i < p1.getPoliom().size(); i++) {
            Monom m1 = p1.getPoliom().get(i);
            for (int j = 0; j < p2.getPoliom().size(); j++) {
                Monom m2 = p2.getPoliom().get(j);
                if (m1.getPutere() == m2.getPutere()) {
                    Monom monom = new Monom(m1.getCoef() + m2.getCoef(), m1.getPutere());
                    result.addTerm(monom);
                    toBeRemoved1.add(m1);
                    toBeRemoved2.add(m2);
                }
            }
        }

        for (Monom m1 : p1.getPoliom())
            result.addTerm(m1);
        for (Monom m2 : p2.getPoliom())
            result.addTerm(m2);

        for (int i = 0; i < toBeRemoved1.size(); i++) {
            result.removeTerm(toBeRemoved1.get(i));
        }
        for (int j = 0; j < toBeRemoved2.size(); j++) {
            result.removeTerm(toBeRemoved2.get(j));
        }


        return clearPolynomial(result);
    }


    public static Polinom substractPolynomials(Polinom p1, Polinom p2) {

        List<Monom> toBeRemoved1 = new ArrayList<>();
        List<Monom> toBeRemoved2 = new ArrayList<>();

        Polinom result = new Polinom();

        for (int i = 0; i < p1.getPoliom().size(); i++) {
            Monom m1 = p1.getPoliom().get(i);
            for (int j = 0; j < p2.getPoliom().size(); j++) {
                Monom m2 = p2.getPoliom().get(j);
                if (m1.getPutere() == m2.getPutere()) {
                    Monom monom = new Monom(m1.getCoef() - m2.getCoef(), m1.getPutere());
                    result.addTerm(monom);
                    toBeRemoved1.add(m1);
                    toBeRemoved2.add(m2);
                }
            }
        }
        for (Monom m1 : p1.getPoliom())
            result.addTerm(m1);
        for (Monom m2 : p2.getPoliom()) {
            m2.setCoef(-1 * m2.getCoef());
            result.addTerm(m2);
        }

        for (int i = 0; i < toBeRemoved1.size(); i++) {
            result.removeTerm(toBeRemoved1.get(i));
        }
        for (int i = 0; i < toBeRemoved2.size(); i++) {
            result.removeTerm(toBeRemoved2.get(i));
        }

        return clearPolynomial(result);
    }

    public static Polinom multiplyPolynomials(Polinom p1, Polinom p2) {

        List<Monom> toBeRemoved1 = new ArrayList<>();
        List<Monom> toBeRemoved2 = new ArrayList<>();

        Polinom result = new Polinom();
        for (int i = 0; i < p1.getPoliom().size(); i++) {
            for (int j = 0; j < p2.getPoliom().size(); j++) {
                Monom monom = new Monom();
                monom.setPutere(p1.getPoliom().get(i).getPutere() + p2.getPoliom().get(j).getPutere());
                monom.setCoef(p1.getPoliom().get(i).getCoef() * p2.getPoliom().get(j).getCoef());
                result.addTerm(monom);
            }
        }
        for (int i = 0; i < result.getPoliom().size() - 1; i++) {
            for (int j = i + 1; j < result.getPoliom().size(); j++) {
                if (result.getPoliom().get(i).getPutere() == result.getPoliom().get(j).getPutere()) {
                    Monom monom = new Monom();
                    monom.setPutere(result.getPoliom().get(i).getPutere());
                    monom.setCoef(result.getPoliom().get(i).getCoef() + result.getPoliom().get(j).getCoef());
                    toBeRemoved1.add(monom);

                }
            }
        }
        for (Monom m1 : p1.getPoliom())
            result.addTerm(m1);
        for (Monom m2 : p2.getPoliom()) {
            m2.setCoef(-1 * m2.getCoef());
            result.addTerm(m2);
        }

        for (int i = 0; i < toBeRemoved1.size(); i++) {
            result.removeTerm(toBeRemoved1.get(i));
        }
        for (int i = 0; i < toBeRemoved2.size(); i++) {
            result.removeTerm(toBeRemoved2.get(i));
        }

        return result;
    }

    public static Polinom derivePolynomial(Polinom p1) {

        for (int i = 0; i < p1.getPoliom().size(); i++) {
            if (p1.getPoliom().get(i).getPutere() == 0) {
                p1.removeTerm(i);
            } else {
                p1.getPoliom().get(i).setCoef(p1.getPoliom().get(i).getCoef() * p1.getPoliom().get(i).getPutere());
                p1.getPoliom().get(i).setPutere(p1.getPoliom().get(i).getPutere() - 1);

            }
        }
        return p1;
    }

    public static Polinom integratePolynomials(Polinom p1) {

        for (int i = 0; i < p1.getPoliom().size(); i++) {
            p1.getPoliom().get(i).setCoef(p1.getPoliom().get(i).getCoef() / (p1.getPoliom().get(i).getPutere() + 1));
            p1.getPoliom().get(i).setPutere(p1.getPoliom().get(i).getPutere() + 1);
        }
        return p1;
    }

    private static Polinom clearPolynomial(Polinom p){

        List<Monom> toBeRemoved = new ArrayList<>();

        Polinom temp = p;
        System.out.println("INAINTE:" + p.toString());
        for(int i = 0; i < p.getPoliom().size(); i++){
            Monom m = temp.getPoliom().get(i);
            if(m.getCoef() == 0){
                m.setCoef(0);
                m.setPutere(0);
                toBeRemoved.add(m);
            }
        }
        System.out.println("DUPA:" + temp.toString());
        for (int i = 0; i < toBeRemoved.size() - 1; i++) {
            temp.removeTerm(toBeRemoved.get(i));
        }
        return temp;

    }
}