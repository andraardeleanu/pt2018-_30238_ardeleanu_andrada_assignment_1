package Model;
import java.util.ArrayList;
import java.util.List;


public class Polinom {

    private List<Monom> polinom;


    public void setPolinom(List<Monom> m) {
        this.polinom = m;
    }


    public List<Monom> getPoliom() {
        return polinom;
    }


    public Polinom(List<Monom> m) {

        polinom = m;
    }


    public Polinom() {
        polinom = new ArrayList<Monom>();
    }


    public void addTerm(Monom m) {
        this.polinom.add(m);
    }


    public void removeTerm(int index) {
        this.polinom.remove(index);
    }

    public void removeTerm(Monom monom) {
        polinom.remove(monom);
    }

    @Override
    public String toString() {
        String equation = "";
        for (int i = 0; i < polinom.size(); i++) {
            if (polinom.get(i).getCoef() > 0)
                equation += "+" + polinom.get(i).getCoef() + "x^" + polinom.get(i).getPutere() + " ";
            else equation += polinom.get(i).getCoef() + "x^" + polinom.get(i).getPutere() + " ";
        }
        return equation;
    }

    public List<Monom> monomSort(List<Monom> polinom) {
        for (int i = 0; i < polinom.size() - 1; i++) {
            for (int j = i + 1; j < polinom.size(); j++) {
                if (polinom.get(i).getPutere() < polinom.get(j).getPutere()) {
                    Monom aux = polinom.get(i);
                    polinom.set(i, polinom.get(j));
                    polinom.set(j, aux);
                }
            }
        }
        return polinom;
    }


    public Polinom monomSort() {
        return new Polinom(monomSort(polinom));
    }

}
