package Model;


public class TestMain  {


    public static void main(String[] arg) {
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();

        p1.addTerm(new Monom(2, 4));
        p1.addTerm(new Monom(4, 3));
        p1.addTerm(new Monom(5, 2));
        p1.addTerm(new Monom(1, 1));
        p1.addTerm(new Monom(2, 0));

        p2.addTerm(new Monom(4, 4));
        p2.addTerm(new Monom(7, 5));
        p2.addTerm(new Monom(2, 2));
        p2.addTerm(new Monom(4, 9));
        p2.addTerm(new Monom(6, 10));


        Polinom result = Operations.addPolynomials(p1, p2);
        System.out.println("Result= " + result.monomSort().toString());
        //System.out.println("java version: "+System.getProperty("java.version"));
        //System.out.println("javafx.version: " + System.getProperty("javafx.version"));


    }


}

