package Model;
public class Monom {

    private float coef;
    private int putere;

    public Monom() {

    }

    public Monom(float coef, int putere) {
        this.coef = coef;
        this.putere = putere;
    }

    public float getCoef() {
        return coef;
    }

    public void setCoef(float coef) {
        this.coef = coef;
    }

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

}

